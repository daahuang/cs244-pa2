#include <iostream>

#include "controller.hh"
#include "timestamp.hh"

using namespace std;

unsigned int min_window_size = 1;

float alpha = 0.1;



/* Default constructor */
Controller::Controller( const bool debug )
  //: debug_( debug ), cur_window_size(1), epoch_start_time(0), prev_avg_ep_max_rtt(0.0), cur_avg_ep_max_rtt(0.0), epoch_max_rtt(0.0)
  : debug_( debug ), max_window_size(1), cur_window_size(1), rtt_avg(-1) , rtt_min(-1), inflight(0), inflight_th(20)
{
    //cur_window_size = 1; // current window size
}




/* Get current window size, in datagrams */
unsigned int Controller::window_size( void )
{
  /* Default: fixed window size of 100 outstanding datagrams */
  //unsigned int the_window_size = 100;
  unsigned int the_window_size = cur_window_size;

  if ( debug_ ) {
    cerr << "At time " << timestamp_ms()
	 << " window size is " << the_window_size << endl;
  }

  return the_window_size;
}

/* A datagram was sent */
void Controller::datagram_was_sent( const uint64_t sequence_number,
				    /* of the sent datagram */
				    const uint64_t send_timestamp )
                                    /* in milliseconds */
{
  /* Default: take no action */

  // calcuate inflight
  inflight += 1;

  if (inflight >= 0.5 * inflight_th ) {cur_window_size = 0;}
  // set window to 0

  if ( debug_ ) {
    cerr << "At time " << send_timestamp
	 << " sent datagram " << sequence_number << endl;
  }
}

/* An ackdijgdbgcukldtckjfghcas received */
void Controller::ack_received( const uint64_t sequence_number_acked,
			       /* what sequence number was acknowledged */
			       const uint64_t send_timestamp_acked,
			       /* when the acknowledged datagram was sent (sender's clock) */
			       const uint64_t recv_timestamp_acked,
			       /* when the acknowledged datagram was received (receiver's clock)*/
			       const uint64_t timestamp_ack_received )
                               /* when the ack was received (by sender) */
{
  /* Default: take no action */

  if ( debug_ ) {
    cerr << "At time " << timestamp_ack_received
	 << " received ack for datagram " << sequence_number_acked
	 << " (send @ time " << send_timestamp_acked
	 << ", received @ time " << recv_timestamp_acked << " by receiver's clock)"
	 << endl;


  }


  /*
  float rtt = (float) (timestamp_ack_received - send_timestamp_acked);
  if (rtt < D_min) D_min = rtt;


  if (epoch_start_time == 0) {
    // first ack
    epoch_start_time = timestamp_ack_received;
    prev_avg_ep_max_rtt = rtt;
    epoch_max_rtt = rtt;

  // decide if still in the same epoch
  } else if (timestamp_ack_received - epoch_start_time > epoch_len) {
    //epoch finished, update stats based on previous epoch_max_rtt and re-initalize list
    cur_avg_ep_max_rtt = alpha * prev_avg_ep_max_rtt + (1 - alpha) * epoch_max_rtt;
    float delta_D = cur_avg_ep_max_rtt - prev_avg_ep_max_rtt;

    // Window estimation
    if ( cur_avg_ep_max_rtt > Ratio * D_min) {
      D_est_next = D_est_cur - delta2;
    } else if (delta_D > 0) {
      D_est_next = D_est_cur - delta1;
      if (D_est_next <  D_min ) {
        D_est_next = D_min;
      }
    } else {
      D_est_next = D_est_cur + delta2;
    }

    // re-initialize list
    epoch_start_time = timestamp_ack_received;
    epoch_max_rtt = rtt;

    // update prev
    prev_avg_ep_max_rtt = cur_avg_ep_max_rtt;
  } else {
    // still in epoch
    if (rtt > epoch_max_rtt) epoch_max_rtt = rtt;
  }
  */


  /* 
  // part C: 
  if ( (timestamp_ack_received - send_timestamp_acked) > 50 ) {// if congested
    cur_window_size = cur_window_size / 2;
    //TODO: slow-start
    if (cur_window_size < min_window_size) {cur_window_size = min_window_size;}
  } else {
    cur_window_size += 1;
  }
  */
  inflight -= 1;
  float rtt = (float) (timestamp_ack_received - send_timestamp_acked);

  if (rtt_min <= 0) rtt_min = rtt; // init
  else {
    if(rtt < rtt_min) rtt_min = rtt;
  }
  //if (rtt_min > 0) {inflight_th = throughput*hrtt_min;}

  if (rtt_avg >= 0 ) {
    rtt_avg = alpha * (rtt -rtt_avg) + rtt_avg;
  } else {
    rtt_avg = rtt;
  }

  /*
  //update max_window_size
  if( rtt > timeout_ms()) max_window_size /= 2;
  else max_window_size += 1;

  if (max_window_size < 1) max_window_size = 1;
  */
  
  if(rtt < 2*rtt_min ) {
    max_window_size += 1;
  }else {
    max_window_size -= 1;
  }
  if (max_window_size > 50) max_window_size = 50;
  if (max_window_size < 1) max_window_size = 1;
  if (true) {
  //if (timestamp_ack_received >= 1000) {
    cur_window_size = min_window_size + (max_window_size - min_window_size) * (rtt_min / rtt_avg);
  }else {
    cur_window_size = max_window_size;
  }
  inflight_th = 3000/rtt_avg;
  unsigned int inflight_diff = inflight_th - inflight;
  if ( inflight_diff > 0  && inflight_diff < cur_window_size ) {
    cur_window_size = inflight_diff;
  }
  //TODO: relation between window_size and inflight?
  
}

// action during timeout
void Controller::timeout_control( void ) {
  max_window_size = 1;
  //max_window_size /= 2;
  cur_window_size = 1;
  //if(max_window_size < 1) max_window_size = 1;
  //cur_window_size = min_window_size + (max_window_size - min_window_size) * (rtt_min / rtt_avg);
}

/* How long to wait (in milliseconds) if there are no acks
   before sending one more datagram */
unsigned int Controller::timeout_ms( void )
{
  return 1.0 * rtt_avg; /* timeout of one second */
  //return 50; /* timeout of one second */
}
